import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import "@/styles/var.less"
createApp(App).use(router).mount("#app")

if (window.name === "name") {
  alert(true)
}
